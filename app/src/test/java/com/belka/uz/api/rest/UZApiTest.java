package com.belka.uz.api.rest;

import com.belka.uz.api.rest.model.SchemeResponse;
import com.belka.uz.api.rest.model.SearchResponse;
import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.api.rest.model.WagonResponse;

import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Volodymyr on 07.12.2017.
 */
public class UZApiTest {

    private UZApi api = new UZApi();

    static Long DNEPR_VALUE = 2210700L;

    @Test
    public void haveTrains() throws Exception {
        Station stationFrom = new Station();
        stationFrom.setStationId(2210833L);

        Station stationTo = new Station();
        stationTo.setStationId(2210700L);

        //boolean result = api.haveTrains(stationFrom, stationTo, new Date());
        assertTrue(false);
    }

    @Test
    public void searchStation() throws Exception {
        List<Station> list = api.searchStations("Днепр");
        assertTrue(DNEPR_VALUE.equals(list.get(1).getStationId()));
    }

    @Test
    public void searchTrains() throws Exception {
        SearchResponse searchResponse = api.searchTrains(2218000L, 2204001L, new Date());
    }

    @Test
    public void getWagons() throws Exception {
        WagonResponse wagonResponse = api.getWagons(2210700L, 2200001L, "012П", "К", 1515972660L);
    }

    @Test
    public void getWagonsByTrainInfo() throws Exception {
        WagonResponse wagonResponse = null;
        SearchResponse searchResponse = api.searchTrains(2218000L, 2204001L, new Date());
        if (!searchResponse.getError()) {
            if (searchResponse.getTrains().size() > 0) {
                TrainInfo trainInfo = searchResponse.getTrains().get(0);
                //wagonResponse = api.getWagons(trainInfo, trainInfo.getPlacesList().get(0).getType());
            }
        }
    }

    @Test
    public void getScheme() throws Exception {
        SchemeResponse schemeResponse = api.getScheme(2210700L, 2200001L, "179П",
                6, "П", "Б", 1514022900L);
    }

    @Test
    public void getWagonsWithSchema() throws Exception {
        WagonResponse wagonResponse = null;
        SearchResponse searchResponse = api.searchTrains(2218000L, 2204001L, new Date());
        if (!searchResponse.getError()) {
            if (searchResponse.getTrains().size() > 0) {
                TrainInfo trainInfo = searchResponse.getTrains().get(0);
                //wagonResponse = api.getWagons(trainInfo, trainInfo.getPlacesList().get(0).getType());
            }
        }
    }

}