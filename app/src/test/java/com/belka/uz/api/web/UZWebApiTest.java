package com.belka.uz.api.web;

import com.belka.uz.api.web.model.WebTrainInfo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Volodymyr on 14.12.2017.
 */
public class UZWebApiTest {

    UZWebApi api = new UZWebApi();
    ScheduleParser parser = new ScheduleParser();

    @Test
    public void getAllTrainByStationName() throws Exception {
        for (WebTrainInfo s : api.getAllTrainByStationName("Геническ", "Львов")) {
            System.out.println(s.toString());
        }
    }

    @Test
    public void getStationWebId() throws Exception {
        String stationWebId = api.getStationWebId("Синельниково 1");
        System.out.println(stationWebId);
    }

    @Test
    public void getAllTrain() throws Exception {
        //http://www.uz.gov.ua/passengers/timetable/?from_station=22700&to_station=22000&select_time=2&time_from=00&time_to=24&by_route=%D0%9F%D0%BE%D1%88%D1%83%D0%BA
        for (WebTrainInfo s : api.getAllTrain("22700", "22000")) {
            System.out.println(s.toString());
        }
    }

    @Test
    public void complexTest() throws WebApiException {
        for (WebTrainInfo s : api.getAllTrain(api.getStationWebId("Днепр-Главный"),
                api.getStationWebId("Киев"))) {
            System.out.println(s.toString());
        }
    }

    @Test
    public void getAllSchedule() throws WebApiException {
        List<String> list = new ArrayList<>();
        for (WebTrainInfo s : api.getAllTrainByStationName("Днепр-Главный", "Киев")) {
                  list.add(s.getSchedule());
                if (!list.contains(s.getSchedule())) {
                }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Днепр-Главный", "Львов")) {
            if (!list.contains(s.getSchedule())) {
                list.add(s.getSchedule());
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Львов", "Киев")) {
            if (!list.contains(s.getSchedule())) {
                list.add(s.getSchedule());
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Львов", "Рахов")) {
            if (!list.contains(s.getSchedule())) {
                list.add(s.getSchedule());
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Днепр-Главный", "Синельниково-1")) {
            if (!list.contains(s.getSchedule())) {
                list.add(s.getSchedule());
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Одесса", "Киев")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Львов", "Одесса")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Одесса", "Львов")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Ужгород", "Киев")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Харьков", "Киев")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (WebTrainInfo s : api.getAllTrainByStationName("Рахов", "Киев")) {
            list.add(s.getSchedule());
            if (!list.contains(s.getSchedule())) {
            }
        }

        for (String s : list) {
            //if (s.contains("парних"))
                System.out.println(s);
                for(String belka : parser.parse(s)) {
                    System.out.println("found = [" + belka+"]");
                }
                System.out.println("----------------------");

        }
    }
}