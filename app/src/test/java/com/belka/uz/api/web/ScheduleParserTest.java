package com.belka.uz.api.web;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Volodymyr on 16.12.2017.
 */
public class ScheduleParserTest {

    ScheduleParser parser = new ScheduleParser();

    @Test
    public void parse() throws Exception {
        for (String str : parser.parse("з 10/12/2017 щоденно,  крім середи")) {
            System.out.println(str);
        }
    }

    @Test
    public void parseDate() throws Exception {
        //середи
        //13/12/2017-30/05/2017,  22/09-8/12/2018 по середах,  суботах
        //23/12/2017-14/01/2018
        List<ScheduleParser.Period> periods = parser.parseDate("13/12/2017-30/05/2017,  22/09-8/12/2018 по середах,  суботах");
        for (ScheduleParser.Period period : periods) {
            System.out.println(period);
        }
        System.out.println("-----------");
        periods = parser.parseDate("середи");
        for (ScheduleParser.Period period : periods) {
            System.out.println(period);
        }
        System.out.println("-----------");
        periods = parser.parseDate("23/12/2017-14/01/2018");
        for (ScheduleParser.Period period : periods) {
            System.out.println(period);
        }
    }

}