package com.belka.uz.api.web;

import com.belka.uz.api.web.model.WebTrainInfo;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Volodymyr on 17.12.2017.
 */
public class TicketsWebApiTest {

    TicketsWebApi api = new TicketsWebApi();

    @Test
    public void getAllTrain() throws Exception {
        //https://gd.tickets.ua/railwaytracker/route_table/2210700/2200001/17.12.2017/0
        for (WebTrainInfo s : api.getAllTrain(2210700L, 2200001L, new Date())) {
            System.out.println(s.toString());
        }
    }

}