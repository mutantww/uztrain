package com.belka.uz;

import com.belka.uz.service.JobService;
import com.belka.uz.service.NotificationService;
import com.belka.uz.service.settings.SettingsService;
import com.belka.uz.service.settings.bean.Settings;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public class Application extends android.app.Application {

    private JobService jobService;
    private NotificationService notificationService;
    private SettingsService settingsService;

    public void onCreate() {
        super.onCreate();
        jobService = new JobService(this);
        settingsService = new SettingsService(this);
        notificationService = new NotificationService(this, jobService, settingsService);
    }

    public JobService getJobService() {
        return jobService;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    public SettingsService getSettingService() {
        return settingsService;
    }

    @Override
    public void onTerminate() {
        notificationService.destroy();
        super.onTerminate();
    }
}
