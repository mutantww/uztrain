package com.belka.uz.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public class JsonIgnoreExclusionStrategy implements ExclusionStrategy {

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(JsonIgnore.class) != null;
    }

    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

}
