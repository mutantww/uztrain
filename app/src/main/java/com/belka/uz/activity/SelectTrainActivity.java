package com.belka.uz.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.belka.uz.R;
import com.belka.uz.viewlist.adapter.TrainListAdapter;
import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.MergingWebApi;
import com.belka.uz.api.web.WebApi;
import com.belka.uz.api.web.WebApiException;
import com.belka.uz.api.web.model.WebTrainInfo;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectTrainActivity extends AppCompatActivity {

    @BindView(R.id.list_of_train)
    ListView listView;

    TrainListAdapter listAdapter;

    WebApi webApi = new MergingWebApi();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_train);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        final Station stationFrom = (Station)intent.getSerializableExtra("stationFrom");
        final Station stationTo = (Station)intent.getSerializableExtra("stationTo");
        final Date date = (Date)intent.getSerializableExtra("date");

        final ProgressDialog progress = new ProgressDialog(SelectTrainActivity.this);
        progress.setTitle("Загрузка");
        progress.setMessage("Подождите...");
        progress.setCancelable(false);
        progress.show();

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                List<WebTrainInfo> list = null;
                try {
                    list = webApi.getAllTrain(stationFrom, stationTo, date);

                    listAdapter = new TrainListAdapter(SelectTrainActivity.this, list);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(listAdapter);
                            progress.dismiss();
                        }
                    });
                } catch (WebApiException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            progress.dismiss();
                            AlertDialog alertDialog = new AlertDialog.Builder(SelectTrainActivity.this).create();
                            alertDialog.setTitle("Ошибка");
                            alertDialog.setMessage("Не удалось получить список поездов");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    });
                            alertDialog.show();
                        }
                    });
                }

            }
        });
    }

}
