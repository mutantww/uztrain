package com.belka.uz.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.belka.uz.Application;
import com.belka.uz.R;
import com.belka.uz.service.settings.SettingsService;
import com.belka.uz.service.settings.bean.Settings;

import butterknife.BindArray;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnItemSelected;

public class SettingsActivity extends AppCompatActivity {

    private SettingsService settingsService;

    @BindArray(R.array.period_seconds_arrays)
    int[] seconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        settingsService = ((Application)getApplication()).getSettingService();
    }

    @OnCheckedChanged(R.id.notification)
    void onGenderSelected(CompoundButton button, boolean checked) {
        Settings settings  = settingsService.getSettings();
        if (settings.isNotification() != checked) {
            settings.setNotification(checked);
            settingsService.saveSettings(settings);
        }
    }

    @OnItemSelected(R.id.period)
    public void spinnerItemSelected(Spinner spinner, int position) {
        if (position >= 0 && position < seconds.length) {
            Settings settings  = settingsService.getSettings();
            if (settings.getPeriod() != seconds[position]) {
                settings.setPeriod(seconds[position]);
                settingsService.saveSettings(settings);
            }
        }
    }
}
