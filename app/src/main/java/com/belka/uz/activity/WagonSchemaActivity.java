package com.belka.uz.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.belka.uz.R;
import com.belka.uz.api.rest.model.Wagon;
import com.belka.uz.fragment.SchemaFragmentPagerAdapter;
import com.belka.uz.fragment.WagonSchemaFragment;

import java.util.List;

public class WagonSchemaActivity extends AppCompatActivity implements WagonSchemaFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wagon_schema);

        // Find the view pager that will allow the user to swipe between fragments
        ViewPager viewPager = findViewById(R.id.viewpager);

        // Create an adapter that knows which fragment should be shown on each page
        Intent intent = getIntent();
        List<Wagon> wagonList = (List<Wagon>) intent.getExtras().get("wagonList");
        SchemaFragmentPagerAdapter adapter = new SchemaFragmentPagerAdapter(this, getSupportFragmentManager(),
                wagonList);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        //TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        //tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
