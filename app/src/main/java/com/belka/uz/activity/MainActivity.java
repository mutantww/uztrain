package com.belka.uz.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.belka.uz.Application;
import com.belka.uz.R;
import com.belka.uz.viewlist.adapter.JobListAdapter;
import com.belka.uz.api.rest.UZApi;
import com.belka.uz.api.rest.model.SearchResponse;
import com.belka.uz.model.Job;
import com.belka.uz.service.JobService;
import com.belka.uz.service.NotificationService;
import com.belka.uz.service.settings.SettingObserver;
import com.belka.uz.service.settings.bean.Settings;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    private JobService jobService;
    private NotificationService notificationService;

    private UZApi api = new UZApi();

    private JobListAdapter listAdapter;

    @BindView(R.id.list_of_job)
    ListView listOfJob;

    private static int ADD_JOB_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        jobService = ((Application)this.getApplication()).getJobService();
        notificationService = ((Application)this.getApplication()).getNotificationService();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent indent = new Intent(MainActivity.this, AddJobActivity.class);
                MainActivity.this.startActivityForResult(indent, ADD_JOB_REQUEST);
            }
        });

        jobService.loadJobs();
        notificationService.Init();


        listAdapter = new JobListAdapter(this, jobService);

        listOfJob.setAdapter(listAdapter);

        final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        final ScheduledFuture[] scheduledFuture = {createScheduledFuture(service, 30)};

        ((Application)getApplication()).getSettingService().addObserver(new SettingObserver() {
            @Override
            public void update(Settings old, Settings neww) {
                scheduledFuture[0].cancel(false);
                scheduledFuture[0] = createScheduledFuture(service, neww.getPeriod());
            }
        });
    }

    ScheduledFuture createScheduledFuture(ScheduledExecutorService service, int seconds) {
        return service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                for (Job job : jobService.getJobsList()) {
                    FindTrainForJob findTrainForJob = new FindTrainForJob();
                    findTrainForJob.execute(job);
                }
            }
        }, 0, seconds, TimeUnit.SECONDS);
    }

    @OnItemClick(R.id.list_of_job)
    public void onItemClick(View view) {
        JobListAdapter.ViewHolder holder = (JobListAdapter.ViewHolder)view.getTag();
        Intent intent = new Intent(view.getContext(), DetailsActivity.class);
        intent.putExtra("uid", holder.getUid());
        view.getContext().startActivity(intent);
    }

        @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putSerializable("jobsList", jobService.getJobsList());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //jobService.setJobsList((ArrayList<Job>) savedInstanceState.getSerializable("jobsList"));
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("выход")
                .setMessage("Хотите выйти из приложения? бот будет остановлен!")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("Нет", null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            this.startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addJob(Job job) {
        jobService.addJob(job);
        FindTrainForJob findTrainForJob = new FindTrainForJob();
        findTrainForJob.execute(job);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_JOB_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Job job = (Job) data.getSerializableExtra("job");
                addJob(job);
                jobService.saveJobs();
                listAdapter.notifyDataSetChanged();
                showNotification(listOfJob, "Добавлено успешно");
            }
        }

    }

    void showNotification(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        listAdapter.notifyDataSetChanged();
    }

    class FindTrainForJob extends AsyncTask<Job, Void, Job> {

        @Override
        protected Job doInBackground(Job... jobs) {
            Job job = jobs[0];
            SearchResponse searchResponse;
            try {
                searchResponse = api.searchTrains(job.getStationFrom(), job.getStationTo(), job.getDate());
                job.setSearchResponse(searchResponse);
            } catch (UZApi.UZApiException e) {
                e.printStackTrace();
                searchResponse = new SearchResponse();
                searchResponse.setError(true);
                searchResponse.setValue("Ошибка подключения к серверу");
            }
            job.setSearchResponse(searchResponse);
            return job;
        }

        @Override
        protected void onPostExecute(Job result) {
            listAdapter.notifyDataSetChanged();
            notificationService.receiveData(result);
        }
    }


}
