package com.belka.uz.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.belka.uz.Application;
import com.belka.uz.R;
import com.belka.uz.viewlist.adapter.StationAutocompleteAdapter;
import com.belka.uz.api.rest.UZApi;
import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.UZWebApi;
import com.belka.uz.api.web.WebApiException;
import com.belka.uz.api.web.model.WebTrainInfo;
import com.belka.uz.model.Job;
import com.belka.uz.service.JobService;
import com.belka.uz.utils.StationUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddJobActivity extends AppCompatActivity {
    private AutoCompleteTextView stationFromView;
    private AutoCompleteTextView stationToView;
    private TextView dateView;

    private SimpleDateFormat dateFormat = new SimpleDateFormat(Job.DATE_FORMAT);

    private Station stationFrom;
    private Station stationTo;
    private Date date;

    private JobService jobService;

    @BindView(R.id.button_select_train)
    TextView selectTrain;

    @BindView(R.id.button_delete_train)
    ImageButton buttonDeleteTrain;

    private WebTrainInfo train;

    public static int SELECT_TRAIN_REQUEST = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);
        ButterKnife.bind(this);

        jobService = ((Application)this.getApplication()).getJobService();

        stationFromView = findViewById(R.id.stationFrom);
        stationToView = findViewById(R.id.stationTo);
        dateView = findViewById(R.id.date);

        date = new Date();
        dateView.setText(dateFormat.format(date));
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateDialog = new DatePicker();
                dateDialog.show(AddJobActivity.this.getFragmentManager(), "datePicker");
            }
        });


        StationAutocompleteAdapter adapter =
                new StationAutocompleteAdapter(this,
                        android.R.layout.simple_dropdown_item_1line, new ArrayList<Station>());
        stationFromView.setAdapter(adapter);

        stationFromView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stationFrom = (Station) view.getTag();
            }

        });

        adapter = new StationAutocompleteAdapter(this,
                        android.R.layout.simple_dropdown_item_1line, new ArrayList<Station>());
        stationToView.setAdapter(adapter);

        stationToView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stationTo = (Station) view.getTag();
            }
        });

    }

    @OnClick(R.id.button_select_train)
    public void selectTrain(final View view) {
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                    if (validateStations(view)) {
                        Intent intent = new Intent(AddJobActivity.this, SelectTrainActivity.class);
                        intent.putExtra("stationFrom", stationFrom);
                        intent.putExtra("stationTo", stationTo);
                        intent.putExtra("date", date);
                        AddJobActivity.this.startActivityForResult(intent, SELECT_TRAIN_REQUEST);
                    }
            }
        });
    }

    @OnClick(R.id.button_delete_train)
    public void deleteTrain(View view) {
        train = null;
        selectTrain.setText("Любой поезд");
        buttonDeleteTrain.setVisibility(View.INVISIBLE);
    }

    private boolean validateStations(final View v) {
        final ProgressDialog[] progress = new ProgressDialog[1];
        runOnUiThread(new Runnable() {
            public void run() {
                progress[0] = new ProgressDialog(AddJobActivity.this);
                progress[0].setMessage("Поиск данных");
                progress[0].setCancelable(false);
                progress[0].show();
            }
        });

        UZApi api = new UZApi();
        UZWebApi uzWebApi = new UZWebApi();
        boolean result = true;

        if (stationFrom == null) {
            if (stationFromView.getText().toString().trim().isEmpty()) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        stationFromView.setError("Введите станция отправления");
                        v.setEnabled(true);
                    }
                });
                result = false;
            } else {
                String stationName = stationFromView.getText().toString().trim();
                List<Station> stationList = Collections.EMPTY_LIST;
                try {
                    stationList = api.searchStations(stationName);
                } catch (UZApi.UZApiException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(v, "Проблема с соединением с сервером", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    });
                }
                Station station = StationUtils.getStationByName(stationName, stationList);
                if (station != null) {
                    stationFrom = station;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            stationFromView.setError("Не удается найти станцию");
                            v.setEnabled(true);
                        }
                    });
                    result = false;
                }

            }
        }

        if (stationTo == null) {
            if (stationToView.getText().toString().trim().isEmpty()) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        stationToView.setError("Введите станция прибытия");
                        v.setEnabled(true);
                    }
                });
                result = false;
            } else {
                String stationName = stationToView.getText().toString().trim();
                List<Station> stationList = Collections.EMPTY_LIST;
                try {
                    stationList = api.searchStations(stationName);
                } catch (UZApi.UZApiException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(v, "Проблема с соединением с сервером", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    });
                }
                Station station = StationUtils.getStationByName(stationName, stationList);
                if (station != null) {
                    stationTo = station;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            stationToView.setError("Не удается найти станцию");
                            v.setEnabled(true);
                        }
                    });

                    result = false;
                }
            }
        }

        try {
            date = dateFormat.parse(dateView.getText().toString());
        } catch (ParseException e) {
            runOnUiThread(new Runnable() {
                public void run() {
                    dateView.setError("Invalid dateView");
                    v.setEnabled(true);
                }
            });
            result = false;
        }

        if (result && stationFrom != null && stationTo != null) {
            try {
                if (!uzWebApi.haveTrains(stationFrom, stationTo, date)) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            AlertDialog alertDialog = new AlertDialog.Builder(AddJobActivity.this).create();
                            alertDialog.setTitle("Ошибка");
                            alertDialog.setMessage("По заданному Вами направлению поездов нет");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    });
                    result = false;
                }
            } catch (WebApiException e) {
                e.printStackTrace();
                result = false;
            }
        }
        runOnUiThread(new Runnable() {
            public void run() {
                progress[0].dismiss();
            }
        });
        return result;
    }

    @OnClick(R.id.add_button)
    public void onClickAddButton(final View v) {
        v.setEnabled(false);
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                    if (validateStations(v)) {
                        final Job job = jobService.newJob();
                        job.setTrain(train);
                        job.setStationFrom(stationFrom);
                        job.setStationTo(stationTo);
                        job.setDate(date);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("job", job);
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                v.setEnabled(true);
                            }
                        });
                    }

            }
        });
    }


    @SuppressLint("ValidFragment")
    public class DatePicker extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            c.setTime(date);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            Dialog picker = new DatePickerDialog(getActivity(), this,
                    year, month, day);
            picker.setTitle("Select date");
            return picker;
        }

        @Override
        public void onStart() {
            super.onStart();
            Button nButton =  ((AlertDialog) getDialog())
                    .getButton(DialogInterface.BUTTON_POSITIVE);
            nButton.setText("OK");

        }

        @Override
        public void onDateSet(android.widget.DatePicker datePicker, int year,
                              int month, int dayOfMonth) {
            date = new GregorianCalendar(year, month, dayOfMonth).getTime();
            dateView.setText(dateFormat.format(date));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_TRAIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                train = (WebTrainInfo) data.getSerializableExtra("train");
                selectTrain.setText("Поезд: " + train.getFullName());
                buttonDeleteTrain.setVisibility(View.VISIBLE);

            }
        }

    }

}
