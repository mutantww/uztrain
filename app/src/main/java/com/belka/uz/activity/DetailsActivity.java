package com.belka.uz.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.belka.uz.Application;
import com.belka.uz.R;
import com.belka.uz.api.rest.UZApi;
import com.belka.uz.api.rest.model.Places;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.api.rest.model.Wagon;
import com.belka.uz.api.rest.model.WagonResponse;
import com.belka.uz.viewlist.adapter.JobListAdapter;
import com.belka.uz.viewlist.adapter.TrainInfoListAdapter;
import com.belka.uz.model.Job;
import com.belka.uz.model.Observer;
import com.belka.uz.service.JobService;
import com.belka.uz.service.NotificationService;
import com.belka.uz.utils.JobUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class DetailsActivity extends AppCompatActivity {

    private SimpleDateFormat dateFormat = new SimpleDateFormat(Job.DATE_FORMAT);

    private Observer<Job> jobObserver;

    private JobService jobService;
    private NotificationService notificationService;

    private UZApi uzApi = new UZApi();

    private Job job;

    @BindView(R.id.train)
    TextView train;

    @BindView(R.id.trainInfoList)
    ListView trainInfoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        jobService = ((Application)this.getApplication()).getJobService();
        notificationService = ((Application)this.getApplication()).getNotificationService();


        final TextView stationFrom = findViewById(R.id.stationFrom);
        final TextView stationTo = findViewById(R.id.stationTo);
        final TextView date = findViewById(R.id.date);

        Intent intent = getIntent();
        final String uid =  intent.getStringExtra("uid");
        job = jobService.get(uid);
        notificationService.removeNotification(job.getUid());

        jobObserver = new Observer<Job>() {
            @Override
            public void update(Job object) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stationFrom.setText(job.getStationFrom().getTitle());
                        stationTo.setText(job.getStationTo().getTitle());
                        if (job.getTrain() != null) {
                            train.setText("Поезд: " + job.getTrain().getFullName() + " " + job.getTrain().getRoute());
                        } else {
                            train.setText("Любой поезд");
                        }
                        date.setText(dateFormat.format(job.getDate()));
                        createInfo(job);
                    }
                });
            }
        };
        job.addObserver(jobObserver);

        jobObserver.update(job);

    }

    @OnItemClick(R.id.trainInfoList)
    public void onItemClick(final View view) {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Загрузка");
        progress.setMessage("Подождите...");
        progress.setCancelable(false);
        progress.show();

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                TrainInfoListAdapter.ViewHolder holder = (TrainInfoListAdapter.ViewHolder) view.getTag();

                final ArrayList<Wagon> wagonList = new ArrayList<>();
                for (Places places : holder.getTrainInfo().getPlacesList()) {
                    try {
                        WagonResponse wagonResponse = uzApi.getWagons(job, holder.getTrainInfo(), places.getType());
                        if (!wagonResponse.getError()) {
                            wagonList.addAll(wagonResponse.getWagonList());
                        }
                    } catch (UZApi.UZApiException e) {
                        e.printStackTrace();
                    }
                }

                Collections.sort(wagonList, Wagon.getComparatorByNumber());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DetailsActivity.this, WagonSchemaActivity.class);
                        intent.putExtra("wagonList", wagonList);
                        progress.dismiss();
                        DetailsActivity.this.startActivity(intent);
                    }
                });

            }
        });
    }

    private void createInfo(Job job) {
        ArrayAdapter infoArrayAdapter;
        if (job.getSearchResponse() != null) {
            if (job.getSearchResponse().getError()) {
                List<String> list = new ArrayList<>();
                list.add(job.getSearchResponse().getMessage());
                infoArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
            } else if (job.getSearchResponse().getAwaitTicketsError()) {
                List<String> list = new ArrayList<>();
                list.add("Билетов еще нет в продаже. Проверьте расписание поездов");
                infoArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
            } else if (job.getSearchResponse().getNoTicketError()) {
                List<String> list = new ArrayList<>();
                list.add("Нет свободных мест");
                infoArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
            } else {
                infoArrayAdapter = new TrainInfoListAdapter(this, JobUtils.getTrains(job));
            }

        } else {
            List<String> list = new ArrayList<>();
            list.add("Информация не доступна");
            infoArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        }
        trainInfoList.setAdapter(infoArrayAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        jobService.removeObserver(jobObserver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.del:
                jobService.remove(job.getUid());
                jobService.saveJobs();
                notificationService.removeNotification(job.getUid());
                finish();
                return true;

            case R.id.change:
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
}
