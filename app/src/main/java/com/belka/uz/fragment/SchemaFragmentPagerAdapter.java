package com.belka.uz.fragment;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;;
import com.belka.uz.R;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.api.rest.model.Wagon;

import java.util.List;

/**
 * Created by volodymyr on 12.01.18.
 */

public class SchemaFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private List<Wagon> wagonList;

    public SchemaFragmentPagerAdapter(Context context, FragmentManager fm, List<Wagon> wagonList) {
        super(fm);
        mContext = context;
        this.wagonList = wagonList;
    }

    @Override
    public Fragment getItem(int position) {
        return WagonSchemaFragment.newInstance(wagonList.get(position));
    }

    @Override
    public int getCount() {
        return wagonList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return wagonList.get(position).getNumber().toString();
    }

}
