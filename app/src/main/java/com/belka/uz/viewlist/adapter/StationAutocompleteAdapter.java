package com.belka.uz.viewlist.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.belka.uz.api.rest.UZApi;
import com.belka.uz.api.rest.model.Station;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Volodymyr on 07.12.2017.
 */

public class StationAutocompleteAdapter extends ArrayAdapter<Station> {
    private final Context mContext;
    private final List<Station> stations;
    private final List<Station> stationsAll;
    private final int textview;
    private UZApi api;

    public StationAutocompleteAdapter(Context context, int layout, List<Station> stations) {
        super(context, layout, stations);
        this.mContext = context;
        this.textview = layout;
        this.stations = new ArrayList<>(stations);
        this.stationsAll = new ArrayList<>(stations);
        api = new UZApi();
    }

    public int getCount() {
        return stations.size();
    }

    public Station getItem(int position) {
        return stations.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(textview, parent, false);
            }
            Station station = getItem(position);
            TextView name = (TextView) convertView.findViewById(android.R.id.text1);
            name.setText(station.getTitle());
            name.setTag(station);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((Station) resultValue).getTitle();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<Station> stationsSuggestion = new ArrayList<>();
                if (constraint != null) {
                    try {
                        stationsSuggestion = new DownloadStation().execute(new String[]{constraint.toString()}).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    filterResults.values = stationsSuggestion;
                    filterResults.count = stationsSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                stations.clear();
                if (results != null && results.count > 0) {
                    for (Object object : (List<?>) results.values) {
                        if (object instanceof Station) {
                            stations.add((Station) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (constraint == null) {
                    stations.addAll(stationsAll);
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    private class DownloadStation extends AsyncTask<String, Void, List<Station>> {

        @Override
        protected List<Station> doInBackground(String... constraint) {
            try {
                return api.searchStations(constraint[0]);
            } catch (UZApi.UZApiException e) {
                return Collections.emptyList();
            }
        }

    }

}