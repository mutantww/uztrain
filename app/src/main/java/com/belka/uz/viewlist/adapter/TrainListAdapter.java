package com.belka.uz.viewlist.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.belka.uz.R;

import com.belka.uz.api.web.model.WebTrainInfo;
import java.util.List;

/**
 * Created by Volodymyr on 16.12.2017.
 */

public class TrainListAdapter extends ArrayAdapter<WebTrainInfo> {

    private final List<WebTrainInfo> trains;
    private final Activity context;

    public TrainListAdapter(Activity context, List<WebTrainInfo> trains) {
        super(context, R.layout.job_layout, trains);
        this.context = context;
        this.trains = trains;
    }

    private static class ViewHolder {
        protected TextView trainNumber;
        //protected TextView trainInfo;
        protected TextView trainRoute;
        protected TextView time;
        protected TextView schedule;
        protected WebTrainInfo train;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        WebTrainInfo currentTrain = trains.get(position);
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.train_layout, null);
            final TrainListAdapter.ViewHolder viewHolder = new TrainListAdapter.ViewHolder();
            viewHolder.trainNumber = view.findViewById(R.id.trainNumber);
            //viewHolder.trainInfo = view.findViewById(R.id.trainInfo);
            viewHolder.trainRoute = view.findViewById(R.id.trainRoute);
            viewHolder.time = view.findViewById(R.id.time);
            viewHolder.schedule = view.findViewById(R.id.schedule);

            view.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    TrainListAdapter.ViewHolder holder = (TrainListAdapter.ViewHolder)view.getTag();
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("train", holder.train);
                    ((Activity)view.getContext()).setResult(Activity.RESULT_OK, resultIntent);
                    ((Activity)view.getContext()).finish();
                }
            });
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        TrainListAdapter.ViewHolder holder = (TrainListAdapter.ViewHolder) view.getTag();
        holder.trainNumber.setText(currentTrain.getFullName());
        //holder.trainInfo.setText(currentTrain.getFullName() + " " + currentTrain.getRoute());
        holder.trainRoute.setText(currentTrain.getRoute());
        holder.time.setText(currentTrain.getDepartureTime1() + " - " + currentTrain.getArriveTime2());
        if (currentTrain.getSchedule() != null && !currentTrain.getSchedule().equals("")) {
            holder.schedule.setText(currentTrain.getSchedule());
            ((TableRow)holder.schedule.getParent()).setVisibility(View.VISIBLE);
        } else {
            ((TableRow)holder.schedule.getParent()).setVisibility(View.GONE);
        }
        holder.train = currentTrain;

        return view;
    }
}
