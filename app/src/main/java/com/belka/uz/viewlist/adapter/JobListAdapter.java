package com.belka.uz.viewlist.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belka.uz.R;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.model.Job;
import com.belka.uz.service.JobService;
import com.belka.uz.utils.JobUtils;

import java.text.SimpleDateFormat;

/**
 * Created by Volodymyr on 06.12.2017.
 */

public class JobListAdapter extends ArrayAdapter<Job>{

    SimpleDateFormat dateFormat = new SimpleDateFormat(Job.DATE_FORMAT);

    private final JobService jobService;
    private final Activity context;

    public JobListAdapter(Activity context, JobService jobService) {
        super(context, R.layout.job_layout, jobService.getJobsList());
        this.context = context;
        this.jobService = jobService;
    }

    public static class ViewHolder {
        protected TextView trainNumber;
        protected TextView stationFrom;
        protected TextView stationTo;
        protected TextView date;
        protected TextView status;
        protected String uid;
        public String getUid() {
            return uid;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        Job currentJob = jobService.get(position);
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.job_layout, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.trainNumber = view.findViewById(R.id.trainNumber);
            viewHolder.stationFrom = view.findViewById(R.id.stationFrom);
            viewHolder.stationTo = view.findViewById(R.id.stationTo);
            viewHolder.date = view.findViewById(R.id.date);
            viewHolder.status = view.findViewById(R.id.status);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.uid = currentJob.getUid();
        if (currentJob.getTrain() != null) {
            holder.trainNumber.setText(currentJob.getTrain().getFullName());
            //((LinearLayout)holder.trainNumber.getParent()).setVisibility(View.VISIBLE);

        } else {
            holder.trainNumber.setText("");
            //((LinearLayout)holder.trainNumber.getParent()).setVisibility(View.GONE);
        }
        holder.stationFrom.setText(currentJob.getStationFrom().getTitle());
        holder.stationTo.setText(currentJob.getStationTo().getTitle());
        holder.date.setText(dateFormat.format(currentJob.getDate()));
        if (currentJob.getSearchResponse() != null) {
            if (currentJob.getSearchResponse().getError()) {
                holder.status.setText(currentJob.getSearchResponse().getMessage());
            } else {
                if (currentJob.getSearchResponse().getAwaitTicketsError()) {
                    holder.status.setText("Билетов еще нет в продаже. Проверьте расписание поездов");
                } else {
                    Integer countPlaces = JobUtils.getFreePlaces(currentJob);
                    String text = countPlaces == 0 ? "Нет свободные мест" : countPlaces.toString() + " мест доступно в поездах: ";
                    for (TrainInfo trainInfo : JobUtils.getTrains(currentJob)) {
                        text += trainInfo.getTrainNumber() + " ";
                    }
                    holder.status.setText(text);
                }
            }
        } else {
            holder.status.setText("");
        }

        return view;
    }
}
