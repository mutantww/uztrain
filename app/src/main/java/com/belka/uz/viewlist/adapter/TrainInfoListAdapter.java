package com.belka.uz.viewlist.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belka.uz.R;
import com.belka.uz.activity.WagonSchemaActivity;
import com.belka.uz.api.rest.model.Places;
import com.belka.uz.api.rest.model.TrainInfo;

import java.util.List;

/**
 * Created by Volodymyr on 16.12.2017.
 */

public class TrainInfoListAdapter extends ArrayAdapter<TrainInfo> {

    private final List<TrainInfo> trains;
    private final Activity context;

    public TrainInfoListAdapter(Activity context, List<TrainInfo> trains) {
        super(context, R.layout.train_info_layout, trains);
        this.context = context;
        this.trains = trains;
    }

    public static class ViewHolder {
        protected TextView trainNumber;
        protected TextView travelTime;
        protected TextView stationFrom;
        protected TextView stationTo;
        protected TextView departureTime;
        protected TextView arriveTime;
        protected TextView freePlaces;
        protected TrainInfo trainInfo;

        public TrainInfo getTrainInfo() {
            return this.trainInfo;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        TrainInfo currentTrain = trains.get(position);
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.train_info_layout, null);
            final TrainInfoListAdapter.ViewHolder viewHolder = new TrainInfoListAdapter.ViewHolder();
            viewHolder.trainNumber = view.findViewById(R.id.trainNumber);
            viewHolder.arriveTime = view.findViewById(R.id.arriveTime);
            viewHolder.departureTime = view.findViewById(R.id.departureTime);
            viewHolder.freePlaces = view.findViewById(R.id.freePlaces);
            viewHolder.stationFrom = view.findViewById(R.id.stationFrom);
            viewHolder.stationTo = view.findViewById(R.id.stationTo);
            viewHolder.travelTime = view.findViewById(R.id.travelTime);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.trainNumber.setText(currentTrain.getTrainNumber());
        holder.departureTime.setText(currentTrain.getStationFrom().getDate());
        holder.arriveTime.setText(currentTrain.getStationTo().getDate());
        holder.travelTime.setText(currentTrain.getTravelTime());
        holder.stationFrom.setText(currentTrain.getStationFrom().getStationName());
        holder.stationTo.setText(currentTrain.getStationTo().getStationName());

        StringBuffer sb = new StringBuffer();
        for (Places places : currentTrain.getPlacesList()) {
            sb.append(places.getName()).append("(" + places.getCount() + ")").append("\n");
        }

        holder.freePlaces.setText(sb.toString());

        holder.trainInfo = currentTrain;

        return view;
    }
}
