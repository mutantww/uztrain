package com.belka.uz.api.web;

import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.model.WebTrainInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by volodymyr on 20.12.17.
 */

public class MergingWebApi implements WebApi {

    private TicketsWebApi ticketsWebApi;

    private UZWebApi uzWebApi;

    public MergingWebApi() {
        ticketsWebApi = new TicketsWebApi();
        uzWebApi = new UZWebApi();
    }

    public List<WebTrainInfo> getAllTrain(Station stationFrom, Station stationTo, Date date) throws WebApiException {
        List<WebTrainInfo> ticketsList = ticketsWebApi.getAllTrain(stationFrom, stationTo, date);

        List<WebTrainInfo> uzList = uzWebApi.getAllTrainByStationName(stationFrom.getTitle(), stationTo.getTitle());

        for(WebTrainInfo trainInfo : ticketsList) {
            int i = uzList.indexOf(trainInfo);
            if (i >= 0) {
                WebTrainInfo t = uzList.get(i);
                while(uzList.remove(t));
                uzList.add(i, t);

                t.setArriveTime1(trainInfo.getArriveTime1());
                t.setDepartureTime1(trainInfo.getDepartureTime1());
                t.setDepartureTime2(trainInfo.getDepartureTime2());
                t.setArriveTime2(trainInfo.getArriveTime2());
                t.setSchedule(trainInfo.getSchedule());
            }
        }

        return uzList;
    }
}
