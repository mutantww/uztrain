package com.belka.uz.api.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Volodymyr on 08.12.2017.
 */
/*
 "num":"235О",
      "model":0,
      "category":0,
      "travel_time":"19:05",
      "from":{
        "station":"Харьков-Пасс",
        "date":1513931220,
        "src_date":"2017-12-22 10:27:00"
      },
      "till":{
        "station":"Ужгород",
        "date":1513999920,
        "src_date":"2017-12-23 05:32:00"
      },
      "types":[
        {
          "id":"К",
          "title":"Купе",
          "letter":"К",
          "places":6
        },
        {
          "id":"П",
          "title":"Плацкарт",
          "letter":"П",
          "places":36
        }
      ],
      "allow_stud":1,
      "allow_transportation":1,
      "allow_booking":1,
      "allow_roundtrip":1
 */
public class TrainInfo implements Serializable {
    @SerializedName("num")
    private
    String trainNumber;
    @SerializedName("travel_time")
    private
    String travelTime;
    @SerializedName("from")
    private
    TrainStation stationFrom;
    @SerializedName("till")
    private
    TrainStation stationTo;
    @SerializedName("types")
    private
    List<Places> placesList;

    public String getTrainNumber() {
        return trainNumber;
    }

    public String getShortTrainNumber() {
        char endChar = trainNumber.charAt(trainNumber.length()-1);
        if (endChar >= '0' && endChar <= '9') {
            return trainNumber;
        }
        return new Integer(trainNumber.substring(0, trainNumber.length()-1)).toString();
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public TrainStation getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(TrainStation stationFrom) {
        this.stationFrom = stationFrom;
    }

    public TrainStation getStationTo() {
        return stationTo;
    }

    public void setStationTo(TrainStation stationTo) {
        this.stationTo = stationTo;
    }

    public List<Places> getPlacesList() {
        return placesList;
    }

    public void setPlacesList(List<Places> placesList) {
        this.placesList = placesList;
    }
}
