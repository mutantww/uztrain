package com.belka.uz.api.rest;

import com.belka.uz.api.rest.model.SchemeResponse;
import com.belka.uz.api.rest.model.SearchResponse;
import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.api.rest.model.TrainStation;
import com.belka.uz.api.rest.model.Wagon;
import com.belka.uz.api.rest.model.WagonResponse;
import com.belka.uz.model.Job;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TransitMode;
import com.google.maps.model.TravelMode;


import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.conn.ssl.NoopHostnameVerifier;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by Volodymyr on 07.12.2017.
 */

public class UZApi {
    public static String DATE_FORMAT = "dd.MM.yyy";

    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    private static String URL_SEARCH_STATION = "https://booking.uz.gov.ua/ru/purchase/station/";
    private static String URL_SEARCH_TRAINS = "https://booking.uz.gov.ua/ru/purchase/search/";
    private static String URL_GET_WAGONS = "https://booking.uz.gov.ua/ru/purchase/coaches/";
    private static String URL_GET_SCHEME_WAGON = "https://booking.uz.gov.ua/ru/purchase/coach/";

    public List<Station> searchStations(String findName) throws UZApiException {
        try {
            HttpResponse response;

            HttpClient client = HttpClientBuilder.create().
                    setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            HttpGet request = new HttpGet();
            URIBuilder uriBuilder = new URIBuilder(new URI(URL_SEARCH_STATION));
            uriBuilder.addParameter("term", findName);
            request.setURI(uriBuilder.build());
            response = client.execute(request);

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Station>>() {}.getType();

            List<Station> stationList = null;

            stationList = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), listType);
            return stationList;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new UZApiException(ex);
        }
    }

    public SearchResponse searchTrains(Station stationFrom, Station stationTo, Date date) throws UZApiException {
        return this.searchTrains(stationFrom.getStationId(), stationTo.getStationId(), date);
    }

    public SearchResponse searchTrains(Long stationIdFrom, Long stationIdTo, Date date) throws UZApiException {
        try {
            HttpResponse response;

            HttpClient client = HttpClientBuilder.create().
                    setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            HttpPost request = new HttpPost(URL_SEARCH_TRAINS);

            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("station_id_from", stationIdFrom.toString()));
            urlParameters.add(new BasicNameValuePair("station_id_till", stationIdTo.toString()));
            urlParameters.add(new BasicNameValuePair("date_dep", dateFormat.format(date)));
            urlParameters.add(new BasicNameValuePair("time_dep", "00:00"));
            urlParameters.add(new BasicNameValuePair("time_dep_till", ""));

            request.setEntity(new UrlEncodedFormEntity(urlParameters, Charset.defaultCharset()));

            response = client.execute(request);

            Gson gson = new Gson();
            SearchResponse searchResponse;

            searchResponse = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), SearchResponse.class);

            return searchResponse;
        } catch (Exception e) {            //response.getEntity().writeTo(System.out);
            e.printStackTrace();
            throw new UZApiException(e);
        }
    }

    public static class UZApiException extends Exception {
        public UZApiException(String messasge) {
            super(messasge);
        }

        public UZApiException(Exception ex) {
            super(ex);
        }
    }

    public WagonResponse getWagons(Job job, TrainInfo trainInfo, String wagonType) throws UZApiException {
        return getWagons(job.getStationFrom().getStationId(), job.getStationTo().getStationId(),
                trainInfo.getTrainNumber(), wagonType, trainInfo.getStationFrom().getTimestamp());
    }

    public WagonResponse getWagonsWithSchema(Job job, TrainInfo trainInfo, String wagonType) throws UZApiException {
        Long departureTime = trainInfo.getStationFrom().getTimestamp();
        WagonResponse wagonResponse = getWagons(job.getStationFrom().getStationId(), job.getStationTo().getStationId(),
                trainInfo.getTrainNumber(), wagonType, departureTime);
        if (!wagonResponse.getError()) {
            for (Wagon wagon: wagonResponse.getWagonList()) {
                wagon.setSchemeResponse(getScheme(job.getStationFrom().getStationId(), job.getStationFrom().getStationId(),
                        trainInfo.getTrainNumber(), wagon.getNumber(), wagon.getType(), wagon.getClazz(), departureTime));
            }

        }
        return wagonResponse;
    }

    public WagonResponse getWagons(Long stationIdFrom, Long stationIdTo, String trainNumber, String wagonType, Long timestamp) throws UZApiException {
        try {
            HttpResponse response;

            HttpClient client = HttpClientBuilder.create().
                    setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            HttpPost request = new HttpPost(URL_GET_WAGONS);

            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("station_id_from", stationIdFrom.toString()));
            urlParameters.add(new BasicNameValuePair("station_id_till", stationIdTo.toString()));

            urlParameters.add(new BasicNameValuePair("train", trainNumber));
            urlParameters.add(new BasicNameValuePair("coach_type", wagonType));
            urlParameters.add(new BasicNameValuePair("date_dep", timestamp.toString()));
            //urlParameters.add(new BasicNameValuePair("round_trip", "0"));
            //urlParameters.add(new BasicNameValuePair("another_ec", "0"));

            request.setEntity(new UrlEncodedFormEntity(urlParameters, Charset.defaultCharset()));

            response = client.execute(request);


            Gson gson = new Gson();
            WagonResponse wagonResponse;

            wagonResponse = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), WagonResponse.class);
            return wagonResponse;
        } catch (Exception e) {
            e.printStackTrace();
            throw new UZApiException(e);
        }
    }

    public SchemeResponse getScheme(Long stationIdFrom, Long stationIdTo, String trainNumber,
                                    Integer wagonNumber, String wagonType, String wagonClass, Long timestamp) throws UZApiException {
        try {
            HttpResponse response;

            HttpClient client = HttpClientBuilder.create().
                    setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            HttpPost request = new HttpPost(URL_GET_SCHEME_WAGON);

            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("station_id_from", stationIdFrom.toString()));
            urlParameters.add(new BasicNameValuePair("station_id_till", stationIdTo.toString()));


            urlParameters.add(new BasicNameValuePair("train", trainNumber));
            urlParameters.add(new BasicNameValuePair("model", "0"));
            urlParameters.add(new BasicNameValuePair("coach_num", wagonNumber.toString()));
            urlParameters.add(new BasicNameValuePair("coach_type", wagonType));
            urlParameters.add(new BasicNameValuePair("coach_class", wagonClass));
            urlParameters.add(new BasicNameValuePair("date_dep", timestamp.toString()));

            request.setEntity(new UrlEncodedFormEntity(urlParameters, Charset.defaultCharset()));

            response = client.execute(request);

            Gson gson = new Gson();
            SchemeResponse schemeResponse;

            //response.getEntity().writeTo(System.out);
            schemeResponse = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), SchemeResponse.class);
            return schemeResponse;
        } catch (Exception e) {
            e.printStackTrace();
            throw new UZApiException(e);
        }
    }


}
