package com.belka.uz.api.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Volodymyr on 13.12.2017.
 */

public class WagonResponse {
    private String content;
    @SerializedName("coaches")
    private
    List<Wagon> wagonList;
    @SerializedName("places_allowed")
    private
    Integer allowedPlaces;
    @SerializedName("places_max")
    private
    Integer maxPlaces;

    private Boolean error;
    private String value;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Wagon> getWagonList() {
        return wagonList;
    }

    public void setWagonList(List<Wagon> wagonList) {
        this.wagonList = wagonList;
    }

    public Integer getAllowedPlaces() {
        return allowedPlaces;
    }

    public void setAllowedPlaces(Integer allowedPlaces) {
        this.allowedPlaces = allowedPlaces;
    }

    public Integer getMaxPlaces() {
        return maxPlaces;
    }

    public void setMaxPlaces(Integer maxPlaces) {
        this.maxPlaces = maxPlaces;
    }

    public Boolean getError() {
        return error != null && error == true;
    }


    public void setError(Boolean error) {
        this.error = error;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
