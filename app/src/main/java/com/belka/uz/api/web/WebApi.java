package com.belka.uz.api.web;

import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.model.WebTrainInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by volodymyr on 20.12.17.
 */

public interface WebApi {
    List<WebTrainInfo> getAllTrain(Station stationFrom, Station stationTo, Date date) throws WebApiException;
}

