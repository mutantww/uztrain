package com.belka.uz.api.rest.model;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by Volodymyr on 14.12.2017.
 */

public class SchemeResponse {

    private Boolean error;

    @JsonAdapter(ValueAdapter.class)
    private Object value;

    public Boolean getError() {
        return error != null && error == true;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getMessage() {
        if (value instanceof String) {
            return (String)value;
        }
        return "";
    }

    public Scheme getScheme() {
        if (value instanceof Scheme) {
            return (Scheme) value;
        }
        return null;
    }

    private static class ValueAdapter extends TypeAdapter {

        @Override
        public void write(JsonWriter jsonWriter, Object trainInfo) throws IOException {
            throw new RuntimeException("Not implemented");
        }

        @Override
        public Object read(JsonReader jsonReader) throws IOException {
            switch (jsonReader.peek()) {
                case STRING:
                    return jsonReader.nextString();

                case BEGIN_OBJECT:
                    return new Gson().fromJson(jsonReader, Scheme.class);

                default:
                    throw new RuntimeException("Expected object or string, not " + jsonReader.peek());
            }
        }
    }
}
