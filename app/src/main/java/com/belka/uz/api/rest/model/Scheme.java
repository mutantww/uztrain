package com.belka.uz.api.rest.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by Volodymyr on 14.12.2017.
 */

public class Scheme {
    @SerializedName("scheme_id")
    private String schemeId;
    @SerializedName("scheme")
    private String content;
    @SerializedName("places")
    private Map<String, List<String>> placeNumbers;

    private transient SchemeBody schemeBody;

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, List<String>> getPlaceNumbers() {
        return placeNumbers;
    }

    public void setPlaceNumbers(Map<String, List<String>> placeNumbers) {
        this.placeNumbers = placeNumbers;
    }

    public SchemeBody getSchemeBody() {
        if (schemeBody == null) {
            Gson gson = new Gson();
            schemeBody = gson.fromJson(content, SchemeBody.class);
        }
        return schemeBody;
    }
}
