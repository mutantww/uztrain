package com.belka.uz.api.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Volodymyr on 08.12.2017.
 */
/*
          "id":"П",
          "title":"Плацкарт",
          "letter":"П",
          "places":36
 */
public class Places implements Serializable {
    @SerializedName("id")
    private
    String type;
    @SerializedName("title")
    private
    String name;
    @SerializedName("places")
    private
    Integer count;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
