package com.belka.uz.api.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

import javax.xml.validation.Schema;

/**
 * Created by Volodymyr on 13.12.2017.
 */

/*
         "num":10,
         "type":"К",
         "railway":32,
         "allow_bonus":false,
         "coach_class":"Б",
         "places_cnt":2,
         "has_bedding":true,
         "reserve_price":1700,
         "services":[
            "Ч",
            "Ш"
         ],
         "prices":{
            "А":30840
         }
 */

public class Wagon implements Serializable {
    @SerializedName("num")
    private
    Integer number;
    @SerializedName("type")
    private
    String type;
    @SerializedName("coach_class")
    private
    String clazz;
    @SerializedName("places_cnt")
    private
    Integer freePlaces;
    @SerializedName("has_bedding")
    private
    Boolean hasBedding;

    private SchemeResponse schemeResponse;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public Integer getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(Integer freePlaces) {
        this.freePlaces = freePlaces;
    }

    public Boolean getHasBedding() {
        return hasBedding;
    }

    public void setHasBedding(Boolean hasBedding) {
        this.hasBedding = hasBedding;
    }

    public static Comparator<Wagon> getComparatorByNumber() {
        return new Comparator<Wagon>() {
            @Override
            public int compare(Wagon w1, Wagon w2)
            {
                return w1.number.compareTo(w2.number);
            }
        };
    }

    public SchemeResponse getSchemeResponse() {
        return schemeResponse;
    }

    public void setSchemeResponse(SchemeResponse schemeResponse) {
        this.schemeResponse = schemeResponse;
    }
}
