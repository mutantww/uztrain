package com.belka.uz.api.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Volodymyr on 08.12.2017.
 */
/*
        "station":"Харьков-Пасс",
        "date":1513931220,
        "src_date":"2017-12-22 10:27:00"
 */
public class TrainStation implements Serializable {
    @SerializedName("station")
    private String stationName;
    @SerializedName("src_date")
    private String date;
    @SerializedName("date")
    private Long timestamp;

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
