package com.belka.uz.api.web;

/**
 * Created by Volodymyr on 19.12.2017.
 */

public class WebApiException extends Exception {
    public WebApiException(Exception ex) {
        super(ex);
    }
    public WebApiException(String message) {
        super(message);
    }
}
