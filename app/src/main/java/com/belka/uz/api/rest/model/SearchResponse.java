package com.belka.uz.api.rest.model;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Volodymyr on 08.12.2017.
 */

/*
  "error":null,
  "data":null,
  "captcha":null
 */
public class SearchResponse implements Serializable {

    private static final String NO_TICKET_ERROR = "По заданному Вами направлению мест нет";
    private static final String NO_TRAINS_ERROR = "По заданному Вами направлению поездов нет";

    private Boolean error;

    @JsonAdapter(ValueAdapter.class)
    private Object value;

    public Boolean getError() {
        return error != null && error == true && !getNoTicketError();
    }

    public boolean getNoTicketError() {
        if (error != null && error == true && !getMessage().isEmpty()
                && (getMessage().trim().equals(NO_TICKET_ERROR) || getMessage().trim().equals(NO_TRAINS_ERROR))) {
            return true;
        }
        return false;
    }

    public boolean getAwaitTicketsError() {
        if (error != null && error == true && !getMessage().isEmpty()
                && getMessage().trim().equals(NO_TRAINS_ERROR)) {
            return true;
        }
        return false;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public List<TrainInfo> getTrains() {
        if (value instanceof List) {
            return (List<TrainInfo>) value;
        }
        return Collections.EMPTY_LIST;
    }

    public String getMessage() {
        if (value instanceof String) {
            return (String)value;
        }
        return "";
    }
}


class ValueAdapter extends TypeAdapter {

    @Override
    public void write(JsonWriter jsonWriter, Object trainInfo) throws IOException {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Object read(JsonReader jsonReader) throws IOException {
        switch (jsonReader.peek()) {
            case STRING:
                return jsonReader.nextString();

            case BEGIN_ARRAY:
                Type listType = new TypeToken<ArrayList<TrainInfo>>(){}.getType();
                return new Gson().fromJson(jsonReader, listType);

            default:
                throw new RuntimeException("Expected object or string, not " + jsonReader.peek());
        }
    }
}
