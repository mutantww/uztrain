package com.belka.uz.api.web;

import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.model.WebTrainInfo;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.conn.ssl.NoopHostnameVerifier;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

/**
 * Created by Volodymyr on 14.12.2017.
 */

public class TicketsWebApi implements WebApi {

    private static String DATE_FORMAT = "dd.MM.yyy";

    private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    private static String URL_GET_TRAINS = "https://gd.tickets.ua/railwaytracker/route_table/{stationFrom}/{stationTo}/{date}/0";

    public List<WebTrainInfo> getAllTrain(Station stationFrom, Station stationTo, Date date) throws WebApiException {
        return getAllTrain(stationFrom.getStationId(), stationTo.getStationId(), date);
    }

    public List<WebTrainInfo> getAllTrain(Long stationIdFrom, Long stationIdTo, Date date) throws WebApiException {
        List<WebTrainInfo> trainList = new ArrayList<>();

        HttpResponse response = null;
        try {
            HttpClient client = HttpClientBuilder.create().
                    setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            HttpGet request = new HttpGet();

            String url = URL_GET_TRAINS.replace("{stationFrom}", stationIdFrom.toString())
                    .replace("{stationTo}", stationIdTo.toString()).replace("{date}", dateFormat.format(date));
            URIBuilder uriBuilder = new URIBuilder(new URI(url));

            request.setURI(uriBuilder.build());
            response = client.execute(request);
            //response.getEntity().writeTo(System.out);

            Pattern pattern = Pattern.compile(
                    "<tr>\\s*" +
                            "<td>\\s*" +
                            "<a\\shref=\"https://gd.tickets.ua/railwaytracker/train/[^>]*>\\s*" +
                            "([0-9]{1,5})\\s*\\(?([^\\(\\)]{0,50})\\)?\\s*</a>\\s*" +
                            "</td>\\s*" +
                            "<td><p><a[^>]*>(.*)</a></p></td>\\s*" +
                            "<td>\\s*" +
                            "<a[^>]*>(.*)</a>\\s*"+
                            "</td>\\s*" +
                            "<td>\\s*-?\\s*([0-9]{1,2}:[0-9]{1,2})?</td>\\s*" +
                            "<td>\\s*-?\\s*([0-9]{1,2}:[0-9]{1,2})?</td>\\s*" +
                            "<td>\\s*" +
                            "<a[^>]*>(.*)</a>\\s*"+
                            "</td>\\s*" +
                            "<td>\\s*-?\\s*([0-9]{1,2}:[0-9]{1,2})?</td>\\s*",
                    Pattern.UNICODE_CASE);

            Scanner scanner = new Scanner(response.getEntity().getContent());

            while(true) {
                String trainStr = scanner.findWithinHorizon(pattern, 0);
                if (trainStr == null)
                    break;
                Matcher matcher = pattern.matcher(trainStr);
                if (matcher.find()) {
                    WebTrainInfo train = new WebTrainInfo();
                    train.setTrainNumber(doFilterString(matcher.group(1)));
                    train.setTrainName(doFilterString(matcher.group(2)));
                    train.setRoute(doFilterString(matcher.group(3)));
                    train.setStationFrom(doFilterString(matcher.group(4)));
                    train.setArriveTime1(doFilterString(matcher.group(5)));
                    train.setDepartureTime1(doFilterString(matcher.group(6)));
                    train.setStationTo(doFilterString(matcher.group(7)));
                    train.setArriveTime2(doFilterString(matcher.group(8)));
                    trainList.add(train);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApiException(e);
        }

        return trainList;
    }

    private static String doFilterString(String str) {
        if (str == null)
            return str;
        if ("".equals(str))
            return null;
        return str;
    }


}
