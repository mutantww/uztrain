package com.belka.uz.api.web.model;

import java.io.Serializable;

/**
 * Created by Volodymyr on 14.12.2017.
 */

public class WebTrainInfo implements Serializable {
    private String trainNumber;
    private String trainName;
    private String route;
    private String schedule;
    private String stationFrom;
    private String arriveTime1;
    private String departureTime1;
    private String stationTo;
    private String arriveTime2;
    private String departureTime2;

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainName() {
        return trainName;
    }

    public String getFullName() {
        return getTrainNumber() + (getTrainName() != null ? ("(" + getTrainName() + ")"): "");
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(String stationFrom) {
        this.stationFrom = stationFrom;
    }

    public String getArriveTime1() {
        return arriveTime1;
    }

    public void setArriveTime1(String arriveTime1) {
        this.arriveTime1 = arriveTime1;
    }

    public String getDepartureTime1() {
        return departureTime1;
    }

    public void setDepartureTime1(String departureTime1) {
        this.departureTime1 = departureTime1;
    }

    public String getStationTo() {
        return stationTo;
    }

    public void setStationTo(String stationTo) {
        this.stationTo = stationTo;
    }

    public String getArriveTime2() {
        return arriveTime2;
    }

    public void setArriveTime2(String arriveTime2) {
        this.arriveTime2 = arriveTime2;
    }

    public String getDepartureTime2() {
        return departureTime2;
    }

    public void setDepartureTime2(String departureTime2) {
        this.departureTime2 = departureTime2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WebTrainInfo trainInfo = (WebTrainInfo) o;

        return trainNumber != null ? trainNumber.equals(trainInfo.trainNumber) : trainInfo.trainNumber == null;
    }

    @Override
    public int hashCode() {
        return trainNumber != null ? trainNumber.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("WebTrainInfo{");
        sb.append("trainNumber='").append(trainNumber).append('\'');
        sb.append(", trainName='").append(trainName).append('\'');
        sb.append(", route='").append(route).append('\'');
        sb.append(", schedule='").append(schedule).append('\'');
        sb.append(", stationFrom='").append(stationFrom).append('\'');
        sb.append(", arriveTime1='").append(arriveTime1).append('\'');
        sb.append(", departureTime1='").append(departureTime1).append('\'');
        sb.append(", stationTo='").append(stationTo).append('\'');
        sb.append(", arriveTime2='").append(arriveTime2).append('\'');
        sb.append(", departureTime2='").append(departureTime2).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
