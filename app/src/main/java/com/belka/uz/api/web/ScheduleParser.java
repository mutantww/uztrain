package com.belka.uz.api.web;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Volodymyr on 16.12.2017.
 */

public class ScheduleParser {

    public static final String DATE_PATTERN = "((цілий\\sрік)|" +
            "(((вівторків((,\\s*)|(\\sта\\s*))?)|(середи((,\\s*)|(\\sта\\s*))?)|(субот((,\\s*)|(\\sта\\s*))?))*" +
            "(([0-9]{1,2},\\s*)*([0-9]{1,2}-[0-9]{1,2},\\s*)*(([0-9]{1,2}/[0-9]{1,2}/[0-9]{4},?\\s*)|([0-9]{1,2}-[0-9]{1,2}/[0-9]{1,2}/[0-9]{4},?\\s*)|-|([0-9]{1,2}/[0-9]{1,2},?\\s*)))*" +
            "((по)\\s*(середах),?\\s*(суботах),?\\s*)*))";

    public static final String DATE_EVERYDAY_DAY = DATE_PATTERN + "\\s*(щоденно)?";

    public static final String FROM_DATE_EVERYDAY_DAY = "з\\s*" + DATE_EVERYDAY_DAY;
    public static final String TO_DATE_EVERYDAY_DAY = "до\\s*" + DATE_EVERYDAY_DAY;
    public static final String DATE_EVERYDAY_DAY_EXCEPT = DATE_EVERYDAY_DAY + ",?\\s*крім\\s*" + DATE_PATTERN;
    public static final String FROM_DATE_EVERYDAY_DAY_EXCEPT = FROM_DATE_EVERYDAY_DAY + ",?\\s*крім\\s*" + DATE_PATTERN;

    public static final String DATE_ODD = DATE_PATTERN + "\\s*по\\s*непарних";
    public static final String DATE_EVEN = DATE_PATTERN + "\\s*по\\s*парних";


    public static final String CANCELING_DATE = "\\s*відмінено\\s*" + DATE_PATTERN;
    public static final String ADDING_DATE = "\\s*додатково\\s*" + DATE_PATTERN;

    public static final String DATE_FROM_DATE_EVERYDAY_DAY = DATE_PATTERN + ",\\s*з\\s*" + DATE_EVERYDAY_DAY;

    public static final String[] DAY_OF_WEEK = {"понеділ", "вівтор", "серед", "четвер", "пяьтні" ,"субот" ,"неділ"};


    public List<String> parse(String str) {

        Pattern patterns[] = {
                Pattern.compile(DATE_EVERYDAY_DAY, Pattern.UNICODE_CASE),
                Pattern.compile(FROM_DATE_EVERYDAY_DAY, Pattern.UNICODE_CASE),
                Pattern.compile(TO_DATE_EVERYDAY_DAY, Pattern.UNICODE_CASE),
                Pattern.compile(DATE_EVERYDAY_DAY_EXCEPT, Pattern.UNICODE_CASE),
                Pattern.compile(FROM_DATE_EVERYDAY_DAY_EXCEPT, Pattern.UNICODE_CASE),
                Pattern.compile(DATE_FROM_DATE_EVERYDAY_DAY, Pattern.UNICODE_CASE),

                Pattern.compile(DATE_ODD, Pattern.UNICODE_CASE),
                Pattern.compile(DATE_EVEN, Pattern.UNICODE_CASE),

                Pattern.compile(CANCELING_DATE, Pattern.UNICODE_CASE),
                Pattern.compile(ADDING_DATE, Pattern.UNICODE_CASE)

        };

        List<String> list = new ArrayList<>();
        for(String str2: str.split(";")) {
            boolean matches = false;
            for (Pattern pattern : patterns) {
                Matcher m = pattern.matcher(str2.trim());
                if (m.matches()) {
                    matches = true;
                    if (m.group(0) != null && !m.group(0).isEmpty()) {
                        list.add(m.group(0).trim());
                    }
                }
            }
            if (!matches)
                break;

        }
        return list;
    }

    public List<Period> parseDate(String dateString) {

        List<Period> periods = new ArrayList<>();

        for (String str : dateString.split(",|(по)")) {
            str = str.trim();
            Period period = new Period();
            if (str.contains("цілий\\sрік")) {
                period.date1 = new DateElement();
                period.date1.fullYear = true;
                period.date2 = period.date1;
                period.setParent(period);
            } else if (str.contains("-")) {
                String date1 = str.split("-")[0];
                String date2 = str.split("-")[1];
                period.date1 = fillDateElement(date1, period);
                period.date2 = fillDateElement(date2, period);
                if (period.date1.month != period.date2.month) {
                    if (period.date1.month == null) {
                        period.date1.month = period.date2.month;
                    }
                }
                if (period.date1.year != period.date2.year) {
                    if (period.date1.year == null) {
                        period.date1.year = new DateTime().getYear();
                    }
                }
            } else if(containsSubStr(DAY_OF_WEEK, str)>0) {
                period.date1 = new DateElement();
                period.date1.dayOfWeek = containsSubStr(DAY_OF_WEEK, str);
                period.date2 = period.date1;
            } else {
                period.date1 = fillDateElement(str, period);
                period.date2= period.date1;
                if (period.date1.isFullDate()) {
                    for (Period period1 : periods)
                        if (period1.parent == null)
                            period1.setParent(period);
                }
            }
            periods.add(period);
        }

        for(int i = periods.size()-1; i>=0; i--) {
            if (periods.get(i).parent == null && !periods.get(i).isFullDate()) {
                for(int j = i-1; j>=0; j--) {
                    if (periods.get(j).isFullDate()) {
                        periods.get(i).setParent(periods.get(j));
                        break;
                    }
                }
            }
        }
        return periods;
    }

    private DateElement fillDateElement(String date, Period period) {
        date = date.trim();
        DateElement dateElement = new DateElement();
        switch(date.split("/").length) {
            case 0:
                break;
            case 1:
                dateElement.day = Integer.parseInt(date);
                break;
            case 2:
                dateElement.day = Integer.parseInt(date.split("/")[0]);
                dateElement.month = Integer.parseInt(date.split("/")[1]);
                break;
            case 3:
                dateElement.day = Integer.parseInt(date.split("/")[0]);
                dateElement.month = Integer.parseInt(date.split("/")[1]);
                dateElement.year = Integer.parseInt(date.split("/")[2]);
                period.parent = period;
                break;
        }
        return dateElement;
    }

    private class DateElement {
        Integer day;
        Integer month;
        Integer year;
        Integer dayOfWeek;
        boolean fullYear;

        public boolean isFullDate() {
            return day!= null && month!= null && year != null || fullYear;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("DateElement{");
            sb.append("date=").append(day).append("/").append(month).append("/").append(year);
            sb.append(", dayOfWeek=").append(dayOfWeek);
            sb.append(", fullYear=").append(fullYear);
            sb.append('}');
            return sb.toString();
        }
    }

    protected class Period {
        DateElement date1;
        DateElement date2;
        Period parent;

        public void setParent(Period parent) {
            this.parent = parent;
        }

        public boolean isFullDate() {
            return date1.isFullDate() || date2.isFullDate();
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Period{");
            sb.append("date1=").append(date1);
            sb.append(", date2=").append(date2);
            sb.append(", parent=").append(parent == this?"this" : parent);
            sb.append('}');
            return sb.toString();
        }
    }

    private Integer containsSubStr(String[] array, String str) {
        int i = 0;
        for(String s : array) {
            if (str.contains(s)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}
