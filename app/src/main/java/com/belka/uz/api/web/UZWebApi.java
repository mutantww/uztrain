package com.belka.uz.api.web;

import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.web.model.WebTrainInfo;
import com.google.gson.Gson;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by Volodymyr on 14.12.2017.
 */

public class UZWebApi implements WebApi {

    private static String URL_GET_TRAINS = "http://www.uz.gov.ua/passengers/timetable/";
    private static String URL_GET_STATION = "http://www.uz.gov.ua/passengers/timetable/suggest-station/";

    private static String EXCLUDE_WAGON = "безпересадковий вагон";

    private static String EVERYDAY= "цілий рік щоденно";

    public String getStationWebId(String stationName) throws WebApiException {
        //http://www.uz.gov.ua/passengers/timetable/suggest-station/?q=%D0%94%D0%BD%D0%B5
        HttpResponse response;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet();
            URIBuilder uriBuilder = new URIBuilder(new URI(URL_GET_STATION));
            uriBuilder.addParameter("q", doFixStationName(stationName));

            request.setURI(uriBuilder.build());
            response = client.execute(request);
            //response.getEntity().writeTo(System.out);

            String responseBody = EntityUtils.toString(response.getEntity());
            if ("false".equals(responseBody.trim())) {
                return null;
            }

            Gson gson = new Gson();
            String[] stations = gson.fromJson(responseBody, String[].class);
            return  stations[0].split("~")[1];
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new WebApiException(ex);
        }

    }

    private String doFixStationName(String stationName) {
        Pattern pattern = Pattern.compile("\\s*(.*)\\s+([0-9]{1,2})\\s*", Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(stationName);

        if (matcher.find()) {
            return matcher.group(1) + "-" + matcher.group(2);
        }
        return stationName;
    }

    public List<WebTrainInfo> getAllTrainByStationName(String stationFromName, String stationToName) throws WebApiException {
        String stationFrom = getStationWebId(stationFromName);
        String stationTo =  getStationWebId(stationToName);

        if (stationFrom != null && stationTo != null)
            return getAllTrain(getStationWebId(stationFromName), getStationWebId(stationToName));

        return new ArrayList<>();
    }

    public List<WebTrainInfo> getAllTrain(String stationWebIdFrom, String stationWebIdTo) throws WebApiException {
        List<WebTrainInfo> trainList = new ArrayList<>();

        HttpResponse response;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet();
            URIBuilder uriBuilder = new URIBuilder(new URI(URL_GET_TRAINS));
            uriBuilder.addParameter("from_station", stationWebIdFrom);
            uriBuilder.addParameter("to_station", stationWebIdTo);
            uriBuilder.addParameter("select_time", "2");
            uriBuilder.addParameter("time_from", "00");
            uriBuilder.addParameter("time_to", "24");
            uriBuilder.addParameter("by_route", "Пошук");

            request.setURI(uriBuilder.build());
            response = client.execute(request);
            //response.getEntity().writeTo(System.out);
/*<tr>	<td><a href="?ntrain=386&amp;by_id=1" title="Інформація по номеру поїзда &quot;41&quot;">41 </a></td>
	<td>Дніпро Трускавець</td> 			<td>цілий рік щоденно</td>
	<td><a href="?station=22700&amp;by_station=1" title="Інформація по станції &quot;Дніпро-Головний&quot;">Дніпро-Головний</a></td>
	<td></td>
	<td>14:25</td>
	<td><a href="?station=47548,23092,23081,23215,36921,23200&amp;by_station=1" title="Інформація по станції &quot;Львів&quot;">Львів</a></td>
	<td>08:36</td>
	<td>08:59</td>
	<!--<td><input type="button" onclick="$('#informationPriceBody').load('/passengers/timetables/?price=41&trf_st=22700&trt_st=23200')" title="Вартість квітка" value="Вартість" /></td>-->
</tr>*/
            Pattern pattern = Pattern.compile(
                    "<tr>.*Інформація\\sпо\\sномеру\\sпоїзда\\s&quot;([0-9]{1,5})&quot;\">[0-9]{1,5}\\s*\\(?([^\\(\\)]{0,30})\\)?</a></td>.*\\n" +
                    ".*<td>(.*)</td>.*<td>(.*)</td>.*\\n" +
                    ".*<td>.*&quot;(.*)&quot;.*</td>.*\\n" +
                    ".*<td>(.*)</td>.*\\n"+
                    ".*<td>(.*)</td>.*\\n" +
                    ".*<td>.*&quot;(.*)&quot;.*</td>.*\\n" +
                    ".*<td>(.*)</td>.*\\n"+
                    ".*<td>(.*)</td>.*\\n",
                    Pattern.UNICODE_CASE);

            Scanner scanner = new Scanner(response.getEntity().getContent());

            while(true) {
                String trainStr = scanner.findWithinHorizon(pattern, 0);
                if (trainStr == null)
                    break;
                Matcher matcher = pattern.matcher(trainStr);
                if (matcher.find()) {
                    WebTrainInfo train = new WebTrainInfo();
                    train.setTrainNumber(doFilterString(matcher.group(1)));
                    train.setTrainName(doFilterString(matcher.group(2)));
                    train.setRoute(doFilterString(matcher.group(3)));
                    train.setSchedule(doFilterString(matcher.group(4)));
                    train.setStationFrom(doFilterString(matcher.group(5)));
                    train.setArriveTime1(doFilterString(matcher.group(6)));
                    train.setDepartureTime1(doFilterString(matcher.group(7)));
                    train.setStationTo(doFilterString(matcher.group(8)));
                    train.setArriveTime2(doFilterString(matcher.group(9)));
                    train.setDepartureTime2(doFilterString(matcher.group(10)));

                    trainList.add(train);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new WebApiException(ex);
        }

        return doFilter(trainList);
    }

    private List<WebTrainInfo> doFilter(List<WebTrainInfo> list) {
        List<WebTrainInfo> newList = new ArrayList<>();
        for (WebTrainInfo train : list) {
            if (//!newList.contains(train) &&
                    !EXCLUDE_WAGON.equals(train.getTrainName())) {
                newList.add(train);
                if (EVERYDAY.equals(train.getSchedule())) {
                    train.setSchedule(null);
                }
            }
        }
        return newList;
    }

    private String doFilterString(String str) {
        if (str == null)
            return str;
        str = str.trim().replaceAll("&quot;", "\"");
        if ("".equals(str))
            return null;
        return str;
    }


    @Override
    public List<WebTrainInfo> getAllTrain(Station stationFrom, Station stationTo, Date date) throws WebApiException {
        return getAllTrain(stationFrom.getTitle(), stationTo.getTitle());
    }

    public boolean haveTrains(Station stationFrom, Station stationTo, Date date) throws WebApiException {
        List<WebTrainInfo> list = this.getAllTrainByStationName(stationFrom.getTitle(), stationTo.getTitle());
        return list.size() > 0;
    }
}
