package com.belka.uz.api.google;

import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TransitMode;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static com.belka.uz.model.Job.DATE_FORMAT;

/**
 * Created by Volodymyr on 15.12.2017.
 */

public class GoogleApi {

    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);


    private GeoApiContext geoApiContext;

    public GoogleApi() {
        geoApiContext = new GeoApiContext.Builder()
                .apiKey("AIzaSyCdWkd6Kw_gbLtv19vd-_cpRVEyMgLfL6A")
                .build();
    }

    public void getTrains() throws ParseException {
        DateTime time = new DateTime(dateFormat.parse("28.12.2017"));

        DirectionsApiRequest request = DirectionsApi.newRequest(geoApiContext)
                .mode(TravelMode.TRANSIT).origin("Днепр-Главный").destination("Киев").arrivalTime(time)
                .transitMode(TransitMode.TRAIN).transitMode(TransitMode.RAIL).optimizeWaypoints(false)
                .alternatives(true);
        try {
            DirectionsResult result = request.await();
            System.out.println(result);
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
