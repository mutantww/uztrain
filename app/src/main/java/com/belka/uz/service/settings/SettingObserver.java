package com.belka.uz.service.settings;

import com.belka.uz.service.settings.bean.Settings;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public interface SettingObserver {
    void update(Settings old, Settings neww);
}
