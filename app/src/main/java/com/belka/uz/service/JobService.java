package com.belka.uz.service;

import com.belka.uz.Application;
import com.belka.uz.model.Job;
import com.belka.uz.model.Observer;
import com.belka.uz.utils.PreferencesUtils;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Volodymyr on 12.12.2017.
 */

public class JobService {
    private ArrayList<Job> jobsList;
    private Type jobsListType = new TypeToken<ArrayList<Job>>(){}.getType();
    private Application application;

    public JobService(Application application) {
        this.application = application;
    }

    public ArrayList<Job> getJobsList() {
        return jobsList;
    }

    public Job newJob() {
        Job job = new Job();
        job.setUid(UUID.randomUUID().toString());
        return job;
    }

    public void addJob(Job job) {
        jobsList.add(job);
    }

    public boolean remove(Job job) {
        return jobsList.remove(job);
    }

    public Job get(String uid) {
        for (Job job : jobsList) {
            if (job.getUid().equals(uid)) {
                return job;
            }
        }
        return null;
    }

    public Job get(int position) {
        return jobsList.get(position);
    }

    public boolean remove(String uid) {
        Job objectToRemove = null;
        for (Job job : jobsList) {
            if (job.getUid().equals(uid)) {
                objectToRemove = job;
            }
        }
        if (objectToRemove != null) {
            return jobsList.remove(objectToRemove);
        }
        return false;
    }

    public void setJobsList(ArrayList<Job> jobsList) {
        this.jobsList = jobsList;
    }

    public void loadJobs() {
        jobsList = new ArrayList<>(PreferencesUtils.getValue(application, "jobsList",
                new ArrayList<Job>(), jobsListType));
    }

    public void saveJobs() {
        PreferencesUtils.writeValue(application, "jobsList", jobsList);
    }

    public void removeObserver(Observer observer) {
        for (Job job : jobsList) {
            job.removeObserver(observer);
        }
    }
}
