package com.belka.uz.service.settings.bean;

import java.io.Serializable;

/**
 * Created by volodymyr on 05.01.18.
 */

public class Settings implements Serializable {
    private int period;
    private boolean notification;

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isNotification() {
        return notification;
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
    }
}
