package com.belka.uz.service.settings;


import com.belka.uz.Application;
import com.belka.uz.service.settings.bean.Settings;
import com.belka.uz.utils.PreferencesUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by volodymyr on 05.01.18.
 */

public class SettingsService {

    private Settings settings;
    private Application application;

    private List<SettingObserver> observerList;


    public SettingsService(Application application) {
        this.application = application;
    }

    public Settings getSettings() {
        if (settings == null) {
            Settings settings = new Settings();
            settings.setPeriod(60);
            settings.setNotification(true);

            settings = PreferencesUtils.getValue(application, "settings", settings, Settings.class);
            this.settings = settings;
        }
        return this.settings;
    }

    public void saveSettings(Settings settings) {
        notifyObservers(settings);
        PreferencesUtils.writeValue(application, "settings", settings);
        this.settings = settings;
    }

    public void addObserver(SettingObserver observer) {
        if (observerList == null)
            observerList = new ArrayList<>();
        observerList.add(observer);
    }

    public void removeObserver(SettingObserver observer) {
        if (observerList != null)
            observerList.remove(observer);
    }

    private void notifyObservers(Settings newSettings) {
        if (observerList != null)
            for(SettingObserver observer : observerList) {
                observer.update(this.settings, newSettings);
            }
    }
}
