package com.belka.uz.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.belka.uz.Application;
import com.belka.uz.R;
import com.belka.uz.activity.DetailsActivity;
import com.belka.uz.model.Job;
import com.belka.uz.model.Notification;
import com.belka.uz.model.NotificationType;
import com.belka.uz.service.settings.SettingsService;
import com.belka.uz.utils.JobUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Volodymyr on 12.12.2017.
 */

public class NotificationService {

    private SimpleDateFormat dateFormat = new SimpleDateFormat(Job.DATE_FORMAT);

    public static final long LIVE_TIME = 10*60*1000;

    private JobService jobService;
    private SettingsService settingsService;
    private Application application;
    private NotificationManager notificationManager;
    private List<Notification> notificationList;

    private List<Job> jobList;

    public NotificationService(Application application, JobService jobService, SettingsService settingsService) {
        this.application = application;
        this.jobService = jobService;
        this.notificationManager = (NotificationManager) this.application.getSystemService(Context.NOTIFICATION_SERVICE);
        this.notificationList = new ArrayList<>();
        this.settingsService = settingsService;
    }

    public void Init() {
        jobList = (List<Job>) jobService.getJobsList().clone();
    }

    public void receiveData(Job job) {
        Job oldJob = getJob(job.getUid());
        if (oldJob == null) {
            jobList.add(job.clone());
            return;
        }
        doNotify(oldJob, job);

        swapJob(oldJob, job);
    }

    private void doNotify(Job oldJob, Job currentJob) {
        if (!settingsService.getSettings().isNotification())
            return ;
        if (oldJob.getSearchResponse() != null && currentJob.getSearchResponse() != null) {
            if (oldJob.getSearchResponse().getError())
                return;
            int oldCount = JobUtils.getFreePlaces(oldJob);
            int currentCount = JobUtils.getFreePlaces(currentJob);
            NotificationType type = null;
            if (currentJob.getSearchResponse().getAwaitTicketsError() && !oldJob.getSearchResponse().getAwaitTicketsError()) {
                type = NotificationType.SALE_TICKETS_STARTED;
            } else if ((currentJob.getSearchResponse().getNoTicketError() || currentCount == 0) && oldCount > 0) {
                type = NotificationType.NO_TICKETS;
            } else if (oldCount > currentCount) {
                type= NotificationType.LESS;
            } else if (currentCount > oldCount) {
                type = NotificationType.MORE;
            } else if ((oldJob.getSearchResponse().getNoTicketError() || oldCount == 0) && currentCount > 0) {
                type = NotificationType.TICKETS_AVAILABLE;
            }
            if (!NotificationType.LESS.equals(type) && type != null) {

                Notification notification = getNotification(currentJob.getUid());
                if (notification != null) {
                    if ((new Date().getTime() - notification.getSendDate().getTime()) > LIVE_TIME
                            || NotificationType.NO_TICKETS.equals(type) || NotificationType.TICKETS_AVAILABLE.equals(type)) {
                        notificationManager.cancel(notification.getNotificationId());
                        notificationList.remove(notification);
                    } else {
                        return;
                    }
                }
                notification = new Notification();
                notification.setJobUid(currentJob.getUid());
                notification.setSendDate(new Date());
                notification.setType(type);
                sendNotification(notification.getNotificationId(), currentJob,
                        createNotificationTitle(notification.getType()),
                        createNotificationText(currentJob, notification.getType()));
                notificationList.add(notification);
            }

        }
    }

    public boolean removeNotification(String uid) {
        Notification notification = getNotification(uid);
        if (notification != null) {
            notificationManager.cancel(notification.getNotificationId());
            return notificationList.remove(notification);
        }
        return false;
    }

    private Notification getNotification(String uid) {
        for (Notification notification : notificationList) {
            if (notification.getJobUid().equals(uid)) {
                return notification;
            }
        }
        return null;
    }

    private String createNotificationText(Job job, NotificationType type) {
        StringBuilder sb = new StringBuilder();
        sb.append(dateFormat.format(job.getDate())).append(" ")
                .append(job.getStationFrom().getTitle()).append(" - ")
                .append(job.getStationTo().getTitle());
        return sb.toString();
    }

    private String createNotificationTitle(NotificationType type) {
        StringBuilder sb = new StringBuilder();
        switch (type) {
            case LESS:
                sb.append("Меньше билетов");
                break;
            case MORE:
                sb.append("Новые билеты в продаже");
                break;
            case NO_TICKETS:
                sb.append("Нет билетов в продаже");
                break;
            case TICKETS_AVAILABLE:
                sb.append("Появились билеты в продаже");
                break;
            case SALE_TICKETS_STARTED:
                sb.append("Началась продажа билетов по направлению");
                break;
        }
        return sb.toString();
    }

    private void swapJob(Job oldJob, Job currentJob) {
        jobList.remove(oldJob);
        jobList.add(currentJob.clone());
    }

    private Job getJob(String uid) {
        for (Job job : jobList) {
            if (job.getUid().equals(uid)) {
                return job;
            }
        }
        return null;
    }

    private void sendNotification(int id, Job job, String title, String text) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this.application)
                        .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                        .setContentTitle(title)
                        .setContentText(text);
        Intent resultIntent = new Intent(this.application, DetailsActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        resultIntent.putExtra("uid", job.getUid());

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this.application,
                0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(android.app.Notification.PRIORITY_MAX);
        mBuilder.setLights(Color.RED, 500, 500);

        mBuilder.setVibrate(new long[]{ 1000, 1000});
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        notificationManager.notify(id, mBuilder.build());
    }

    public void destroy() {
        notificationManager.cancelAll();
    }
}
