package com.belka.uz.utils;

import com.belka.uz.api.rest.model.Station;

import java.util.List;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public class StationUtils {
    public static Station getStationByName(String stationName, List<Station> stationList) {
        for (Station station : stationList) {
            if (station.getTitle().equalsIgnoreCase(stationName)) {
                return station;
            }
        }
        return null;
    }
}
