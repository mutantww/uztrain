package com.belka.uz.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.belka.uz.gson.JsonIgnoreExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * Created by Volodymyr on 10.12.2017.
 */

public class PreferencesUtils {
    public static void writeValue(Context context, String key, Object value) {
        SharedPreferences sharedPref = context.getSharedPreferences("com.belka.uz",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new GsonBuilder().setExclusionStrategies(new JsonIgnoreExclusionStrategy()).create();
        editor.putString(key, gson.toJson(value));
        editor.commit();
    }

    public static <T> T getValue(Context context, String key, T defValue, Type type) {
        SharedPreferences sharedPref = context.getSharedPreferences("com.belka.uz", Context.MODE_PRIVATE);
        String value = sharedPref.getString(key, "");
        if (value.equals("")) {
            return defValue;
        }
        Gson gson = new GsonBuilder().setExclusionStrategies(new JsonIgnoreExclusionStrategy()).create();
        return gson.fromJson(value, type);
    }

}
