package com.belka.uz.utils;

import com.belka.uz.api.rest.model.Places;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.model.Job;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Volodymyr on 12.12.2017.
 */

public class JobUtils {
    public static int getFreePlaces(Job job) {
        Integer count = 0;
        if (!job.getSearchResponse().getNoTicketError() && job.getSearchResponse() != null)
        for (TrainInfo train : job.getSearchResponse().getTrains()) {
            if (job.getTrain() == null || job.getTrain().getTrainNumber().equals(train.getShortTrainNumber())) {
                for (Places places : train.getPlacesList()) {
                    count += places.getCount();
                }
            }
        }
        return count;
    }

    public static List<TrainInfo> getTrains(Job job) {
        List<TrainInfo> trains = new ArrayList<>();
        if (!job.getSearchResponse().getNoTicketError() && job.getSearchResponse() != null)
            for (TrainInfo train : job.getSearchResponse().getTrains()) {
                if (job.getTrain() == null || job.getTrain().getTrainNumber().equals(train.getShortTrainNumber())) {
                    trains.add(train);
                }
            }
        return trains;
    }
}
