package com.belka.uz.model;

import com.belka.uz.api.rest.model.SearchResponse;
import com.belka.uz.api.rest.model.Station;
import com.belka.uz.api.rest.model.TrainInfo;
import com.belka.uz.api.web.model.WebTrainInfo;
import com.belka.uz.gson.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Volodymyr on 06.12.2017.
 */

public class Job implements Serializable, Observable, Cloneable {

    public static String DATE_FORMAT = "dd.MM.yyy";

    private String uid;
    private Station stationFrom;
    private Station stationTo;
    private Date date;
    private WebTrainInfo train;
    @JsonIgnore
    private SearchResponse searchResponse;

    private transient List<Observer> observerList;

    public Station getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(Station stationFrom) {
        this.stationFrom = stationFrom;
    }

    public Station getStationTo() {
        return stationTo;
    }

    public void setStationTo(Station stationTo) {
        this.stationTo = stationTo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public SearchResponse getSearchResponse() {
        return searchResponse;
    }

    public void setSearchResponse(SearchResponse searchResponse) {
        this.searchResponse = searchResponse;
        this.notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        if (observerList == null)
            observerList = new ArrayList<>();
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (observerList != null)
            observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        if (observerList != null)
        for(Observer observer : observerList) {
            observer.update(this);
        }
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public Job clone() {
        Job job = null;
        try {
            job = (Job) super.clone();
            job.observerList = null;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return job;
    }

    public WebTrainInfo getTrain() {
        return train;
    }

    public void setTrain(WebTrainInfo train) {
        this.train = train;
    }
}
