package com.belka.uz.model;

import java.util.Date;

/**
 * Created by Volodymyr on 12.12.2017.
 */

public class Notification {

    private static int NOTIFICATION_ID_COUNTER = 0;

    private String jobUid;
    private int notificationId;
    private NotificationType type;
    private Date sendDate;

    public Notification() {
        NOTIFICATION_ID_COUNTER++;
        this.notificationId = NOTIFICATION_ID_COUNTER;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public String getJobUid() {
        return jobUid;
    }

    public void setJobUid(String jobUid) {
        this.jobUid = jobUid;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
}
