package com.belka.uz.model;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public interface Observable {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}
