package com.belka.uz.model;

/**
 * Created by Volodymyr on 12.12.2017.
 */

public enum NotificationType {
    LESS,
    MORE,
    NO_TICKETS,
    TICKETS_AVAILABLE,
    SALE_TICKETS_STARTED,
    OPEN_WAGON
}
