package com.belka.uz.model;

/**
 * Created by Volodymyr on 11.12.2017.
 */

public interface Observer<T> {
    void update(T object);
}
